const express = require('express')

//initialize the application using express
const app = express()

//declare what port will be used by the application, what the server will give if not, run in 3000
const port = process.env.PORT || 3000

const products = require('./routes/product');

//middleware

//application level middleware
app.use((req,res,next)=>{
	//console.log("Hello! My nagrequest");
	console.log(req.method);
	next();
});

//Parse incoming request body to json format
//this will create a body attribute to the request object
app.use(express.json());


	//access /products then run product function
app.use('/products', products)




//route middleware
//middlewareFunction accepts up to 3 parameters
//req - object containing all the info about the request
//res - response, 
//next - can still pass to the other middleware
// app.httpMETHOD('/path(route)', middlewareFunction(req,res){

// })

app.get('/', (req, res) => {
  res.send('Hello World!!')
})



//this will run the application on the desired port
app.listen(port, () => {
  console.log(`App is listening at port ${port}`)
})