const router = require('express').Router();
//initialize

//router level middleware
router.use((req,res,next)=>{
	//console.log(req);
	console.log(req.method + " " + req.originalUrl + " " + new Date(Date.now()))
	next()
})

router.get('/', (req, res) => {
	res.json({
		message: "List of all products"
	})
})


router.get('/:id', (req, res) => {
	// console.log(req.params)
	res.json({
		message: "View specific product with id " + req.params.id
	})
})


router.post('/', (req,res) => {

	//console.log(req.body);
	res.json({
		message: req.body
	})
})

router.put('/:id', (req,res) => {
	res.json({
		message: req.body
	})
})

router.delete('/:id', (req,res) => {
	res.json({
		message: "This is delete product endpoint"
	})
})


module.exports = router;
//once require, export. And must be at the bottom most part